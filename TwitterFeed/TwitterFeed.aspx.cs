﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LinqToTwitter;
using System.Threading;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace TwitterFeed
{
    public partial class TwitterFeed : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           string output = getTwitterFeed("ISUSOC", 5);

           twitterFeed.InnerHtml = output;
        }
         public string getTwitterFeed (string screenName, int count)
        {
            // This is a super simple example that
            // retrieves the latest tweets of a given 
            // twitter user.

            // SECTION A: Initialise local variables
            Console.WriteLine("SECTION A: Initialise local variables");

            const string accessToken = "Access token goes here .. (Please generate your own)";
            const string accessTokenSecret = "Access token secret goes here .. (Please generate your own)";
            const string consumerKey = "Api key goes here .. (Please generate your own)";
            const string consumerSecret = "Api secret goes here .. (Please generate your own)";

            // The twitter account name goes here
             string twitterAccountToDisplay = screenName;


            // SECTION B: Setup Single User Authorisation
            Console.WriteLine("SECTION B: Setup Single User Authorisation");
            var authorizer = new SingleUserAuthorizer
            {
                CredentialStore = new InMemoryCredentialStore
                {
                    ConsumerKey = consumerKey,
                    ConsumerSecret = consumerSecret,
                    OAuthToken = accessToken,
                    OAuthTokenSecret = accessTokenSecret
                }
            };

            // SECTION C: Generate the Twitter Context
            Console.WriteLine("SECTION C: Generate the Twitter Context");
            var twitterContext = new TwitterContext(authorizer);

            // SECTION D: Get Tweets for user
            Console.WriteLine("SECTION D: Get Tweets for user");
            var statusTweets = from tweet in twitterContext.Status
                               where tweet.Type == StatusType.User &&
                                       tweet.ScreenName == twitterAccountToDisplay &&
                                       tweet.IncludeContributorDetails == true &&
                                       tweet.Count == count &&
                                       tweet.IncludeEntities == true
                               select tweet;

            // SECTION E: Print Tweets
            Console.WriteLine("SECTION E: Print Tweets");
            string tweetOutPut = PrintTweets(statusTweets);
            

            return tweetOutPut;
        }//end getTwitterFeed

        /// <summary>
        /// Prints the tweets.
        /// </summary>
        /// <param name="statusTweets">The status tweets.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        private string PrintTweets(IQueryable<Status> statusTweets)
        {
            string tweet = "<div class='twitter-feed-holder'>";
            string name = "";
            foreach (var statusTweet in statusTweets)
            {
                name = statusTweet.ScreenName;
                string profileImage = statusTweet.User.ProfileImageUrl;
                string media = "";
                
                int mediaWidth = 0;
                int mediaHeight = 0;
                string dateTime = "";
                Boolean styleDates = true;
                TimeSpan createdAtDiff = new TimeSpan();
                DateTime createdAt = statusTweet.CreatedAt;
                string tweetDesc = statusTweet.Text;
                    //Regular Expressions to surround the links with anchors(Not Working Yet)
                string exp1 = @"(https?://([-\w\.]+)+(:\d+)?(/([\w/_\.]*(\?\S+)?)?)?)";
                string exp2 = @"@(\w+)";
                string exp3 = @"#(\w+)";

                            tweetDesc = Regex.Replace(tweetDesc, exp1, "<a href=\"$0\" target=\"_blank\">$0</a>" );
							tweetDesc = Regex.Replace(tweetDesc, exp2, "<a href=\"http://twitter.com/$1\" target=\"_blank\">$0</a>" );
							tweetDesc = Regex.Replace(tweetDesc, exp3, "<a href=\"http://twitter.com/search?q=%23$1\" target=\"_blank\">$0</a>");

                            if (tweetDesc[0] == 'R' && tweetDesc[1] == 'T')
                            {
                                var regex = new Regex(Regex.Escape("RT"));
                                tweetDesc = regex.Replace(tweetDesc, "<span class='icon-retweet'><span class='visuallyhidden'>Retweeted</span></span>", 1);
                            }


                if (statusTweet.Entities.MediaEntities.Count > 0)
                {
                    media = statusTweet.Entities.MediaEntities[0].MediaUrl;

                    if (statusTweet.Entities.MediaEntities[0].Type == "photo")
                    {
                        //0 = 600w  x 450h - medium
                        //1 = 1024w x 768h - Large
                        //2 = 150w  x 150h - thumb
                        //3 = 340w  x 255h - small
                        mediaWidth = statusTweet.Entities.MediaEntities[0].Sizes[3].Width;
                        mediaHeight = statusTweet.Entities.MediaEntities[0].Sizes[3].Height;
                    }
                }
                if (styleDates)
                {
                    // Current UNIX timestamp.
                    DateTime current_time = DateTime.Now;
                    createdAtDiff = current_time.Subtract(createdAt);
                    if (createdAtDiff <= TimeSpan.FromSeconds(60))
                        dateTime = string.Format("{0} seconds ago", createdAtDiff.Seconds);

                    else if (createdAtDiff <= TimeSpan.FromMinutes(60))
                        dateTime = createdAtDiff.Minutes > 1 ? String.Format("about {0} minutes ago", createdAtDiff.Minutes) : "about a minute ago";

                    else if (createdAtDiff <= TimeSpan.FromHours(24))
                        dateTime = createdAtDiff.Hours > 1 ? String.Format("about {0} hours ago", createdAtDiff.Hours) : "about an hour ago";

                    else if (createdAtDiff <= TimeSpan.FromDays(30))
                        dateTime = createdAtDiff.Days > 1 ? String.Format("about {0} days ago", createdAtDiff.Days) : "yesterday";

                    else if (createdAtDiff <= TimeSpan.FromDays(365))
                        dateTime = createdAtDiff.Days > 30 ? String.Format("about {0} months ago", createdAtDiff.Days / 30) : "about a month ago";
                    else
                    {
                        dateTime = createdAtDiff.Days > 365 ? String.Format("about {0} years ago", createdAtDiff.Days / 365) : "about a year ago";
                    }
                    
                }




                
                //Console.WriteLine("width: " + mediaWidth + " height: " +mediaHeight);
                if (media == "")
                {
                    tweet += "<div class='twitter-feed-item'>";
                    tweet += "<img class='twitter-feed-profile-image' src='" + profileImage + "'alt='"+name+" Profile Image' style='float:left; width: 25px;height: auto;'/>";
                    tweet += "<p class='twitter-feed-text'>";
                    tweet += tweetDesc;
                    tweet += "<//p>";
                    tweet += "<span class='twitter-feed-created'>" + dateTime + "</span>";
                    tweet += "</div>";
                }
                else
                {
                    if (statusTweet.Entities.MediaEntities[0].Type == "photo")
                    {
                        tweet += "<div class='twitter-feed-item'>";
                        tweet += "<img class='twitter-feed-profile-image' src='" + profileImage + "'alt='" + name + " Profile Image' style='width: 25px;height: auto;'/>";
                        tweet += "<p class='twitter-feed-text'>";
                        tweet += tweetDesc;
                        tweet += "</p>";
                        tweet += "<button class='twitter-feed-view-media'>View Media</button>";
                        tweet += "<img class='tweet-media' width='" + mediaWidth + "' height='" + mediaHeight + "' src='" + media + "' alt='"+tweetDesc+"'/>";
                        tweet += "<span class='twitter-feed-created'>" + dateTime + "</span>";
                        tweet += "</div>";
                    }
                    else
                    {
                        //media type equals video (still need to see how a video looks in the json before able to complete this code
                        tweet += "<div class='twitter-feed-item'>";
                        tweet += "<img class='twitter-feed-profile-image' src='" + profileImage + "'alt='" + name + " Profile Image' style='width: 25px;height: auto;'/>";
                        tweet += "<p class='twitter-feed-text'>";
                        tweet += tweetDesc;
                        tweet += "</p>";
                        tweet += "<span class='twitter-feed-created'>" + dateTime + "</span>";
                        tweet += "</div>";
                    }

                }

                Console.WriteLine(tweet);
                Thread.Sleep(1000);
                
            }
            string followURL = "https://twitter.com/" + name;
            tweet += "</div>";
            tweet += "<a href='"+followURL+"' class='twitter-follow-button' data-show-count='false'>Follow @"+name+"</a>";
            tweet += "<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>";
            tweet += "<script>$(document).ready(function(){$('.tweet-media').css('display','none');$('.twitter-feed-view-media').click(function(){$(this).next('.tweet-media').fadeToggle('slow');});});</script>";
            
            return tweet;
        }
    }
}