﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TwitterFeed.aspx.cs" Inherits="TwitterFeed.TwitterFeed" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>

<script runat="server">
    
    string responseText;
    private void Page_Load(object sender, System.EventArgs e)
    {
        responseText = getTwitterFeed("vision_youtube", 5);
    }
</script>

</head>
<body>


    <form id="form1" runat="server">
        <div id="twitterFeed" class="twitterFeed" runat="server">
            <%= responseText %>
        </div>
    </form>
</body>
</html>
